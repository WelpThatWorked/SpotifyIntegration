﻿using UnityEngine;
using SpotifyAPI.Local; //Base Namespace
using SpotifyAPI.Local.Enums; //Enums
using SpotifyAPI.Local.Models; //Models for the JSON-responses
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Models;
using SpotifyAPI.Web.Enums;

public class SpotifyStuff : MonoBehaviour
{
    private static SpotifyWebAPI spotifyWeb = new SpotifyWebAPI();
    private static SpotifyLocalAPI spotifyLocal = new SpotifyLocalAPI();
    public async System.Threading.Tasks.Task<bool> Setup()
    {
        if (!SpotifyLocalAPI.IsSpotifyRunning())
        { 
            print("Spotify Not Running!"); //Make sure the spotify client is running
            SpotifyLocalAPI.RunSpotify();
        }
        if (!SpotifyLocalAPI.IsSpotifyWebHelperRunning())
        {
            print("WebHelper Not Running!"); //Make sure the WebHelper is running
            SpotifyLocalAPI.RunSpotifyWebHelper();
        }
        if (!spotifyLocal.Connect())
            print("Connection Failed!"); //We need to call Connect before fetching infos, this will handle Auth stuff

        StatusResponse status = spotifyLocal.GetStatus(); //status contains infos
        
        WebAPIFactory webApiFactory = new WebAPIFactory(
      "http://localhost",
      8000,
      "XXXXXXXXXXXXXXXX",
      Scope.UserReadPrivate,
      System.TimeSpan.FromSeconds(20)
 );

        try
        {
            //This will open the user's browser and returns once
            //the user is authorized.
            spotifyWeb = await webApiFactory.GetWebApi();
            return true;
        }
        catch (System.Exception ex)
        {
            print(ex.Message);
            return false;
        }
        
    }
    public async System.Threading.Tasks.Task<bool> Play(string url)
    {
        try
        {
            await spotifyLocal.PlayURL(url, "");
            return true;
        }
        catch(System.Exception e)
        {
            print(e.Message);
            return false;
        }
    }

    void Start()
    {
        var foo = Setup();
        print("potato" );
        Play("spotify:track:2374M0fQpWi3dLnB54qaLX");
        
        print("potato");
        //...
    }

    void Update()
    {

    }

    void OnGUI()
    {

    }
}