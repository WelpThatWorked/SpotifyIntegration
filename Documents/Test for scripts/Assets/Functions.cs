﻿using System.Collections;
using UnityEngine;

public class Functions : MonoBehaviour
{
    
    public AudioClip ImportAudio(string url)
    {
        if (url.EndsWith("mp3"))
        {

            WWW w = new WWW(url);
             return NAudioPlayer.FromMp3Data(w.bytes);
        }
        else
        {

            WWW w = new WWW(url);
            return w.GetAudioClip();
        }
    }

    void Start()
    {

        var foo = GetComponent<AudioSource>();
        foo.clip = ImportAudio("C:/Users/Jennifer/Downloads/small.ogg");
        foo.Play();
    }

    void Update()
    {
        
    }

    void OnGUI()
    {
        
    }
}